﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace Ej_binding.Modelo
{
    public class Empleado : INotifyPropertyChanged
    {

        public string Apellidos { get; set; }
        public int Salario { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        string _nombre;
        public string Nombre
        {
            get { return _nombre; }
            set
            {
                if (value.Equals(_nombre, StringComparison.Ordinal))
                {
                    // no hacemos nada porque el valor no se cambia
                    return;
                }
                else
                {
                    _nombre = value;
                    OnPropertyChanged();
                }
            }
        }

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
