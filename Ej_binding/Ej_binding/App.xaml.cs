﻿using Ej_binding.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Ej_binding
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

            //MainPage = new Ej_binding.MainPage();

            var e = new Empleado
            {
                Nombre = "Neftalí",
                Apellidos = "Espinosa González",
                Salario = 45000
            };

            MainPage = new EmpleadoPage(e);
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
