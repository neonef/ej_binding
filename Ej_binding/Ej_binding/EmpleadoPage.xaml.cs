﻿using Ej_binding.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ej_binding
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EmpleadoPage : ContentPage
	{
		public EmpleadoPage (Empleado emp)
		{
			InitializeComponent ();

            this.BindingContext = emp;
		}
	}
}